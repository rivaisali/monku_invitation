<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $domain = str_replace("https://","",base_url());
        $invitation = $this->model->select_data('invitations', 'getRow', ["full_domain" => $domain]);
        $data['data'] = $this->model->select_data('invitations', 'getRow', ["full_domain" => $domain]);
        $data['header'] = $this->model->select_data('gallery', 'getRow', ["invitation_id" => $invitation->id, "category" => "header"]);
        $data['event'] = $this->model->select_data('events', 'getResult', ["invitation_id" => $invitation->id]);
        $data['gallery'] = $this->model->select_data('gallery', 'getResult', ["invitation_id" => $invitation->id, "category" => "basic"]);
        return view('main', $data);
    }
}
