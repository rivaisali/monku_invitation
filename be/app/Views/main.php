<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->
<head>
    <meta charset="utf-8" />
    <title><?=$data->bride_nickname?> &amp; <?=$data->groom_nickname?> | Undangan Pernikahan</title>
    <meta name="keywords"
        content="Desain Undangan Pernikahan Online Gratis, Website Pernikahan, Wedding Website, Desain Undangan, Invitation Design" />
    <meta name="author" content="toduwo.id">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta property="og:title" content="Undangan Pernikahan Anto &amp; Desy |  AntoDesy.toduwo.id">
    <!--<meta property="og:image" content="<?php echo base_url() ?>assets/img/og_image.jpg">-->
    <meta name="twitter:card" content="summary_large_image">
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CGreat+Vibes" rel="stylesheet"
        type="text/css">
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="<?php base_url() ?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="<?php base_url() ?>/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="<?php base_url() ?>/assets/css/layout/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/css/layout/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/css/layout/brand/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/css/layout/aside/light.css" rel="stylesheet" type="text/css" />
    <link href="<?php base_url() ?>/assets/css/custom.css" rel="stylesheet" type="text/css" />
     <!--end::Layout Themes-->
     <link rel="shortcut icon" href="<?php base_url() ?>/assets/images/logo-icon.png">
    <link href="<?php base_url() ?>/assets/css/instahistory.css" rel="stylesheet" type="text/css" />
    <?php
    $primaryColor= "#dea095";
    $dir_theme = "theme/rose_v2";
    $base_url = "https://demo.monku.id/";
    ?>
    <link href="<?php base_url() ?>/assets/css/style.php" rel="stylesheet" type="text/css" />
   
   
</head>
<!--end::Head-->
<!--begin::Body-->
<!--</body></html>-->

<body id="kt_body" class="">
    <!--begin::Main-->
    <!--begin::Header Mobile-->
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            <!--begin::Aside-->
            <!--end::Aside-->
            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                <!--begin::Header-->
                <!--end::Header-->
                <!--begin::Navigation-->
                <div class="footer bg-white py-4 d-flex flex-lg-column" id="bottom-navigation">

                    <!--begin::Container-->
                    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-center">
                        <!--begin::Nav-->

                        <div class="d-flex align-items-center">
                            <!--begin::Actions-->
                            <a href="#main" class="btn btn-light-theme btn-hover-theme mr-2 color-gold_green">
                                <i class="flaticon-home-2"></i><br>
                                <span>Home</span>
                            </a>

                            <a href="#bridegroom" class="btn btn-light-theme btn-hover-theme mr-2">
                                <i class="far fa-heart"></i><br>
                                <span>Detail</span>
                            </a>

                            <a href="#event" class="btn btn-light-theme btn-hover-theme mr-2">
                                <i class="flaticon-calendar-with-a-clock-time-tools"></i><br>
                                <span>Acara</span>
                            </a>
                            <a href="#comment" class="btn btn-light-theme btn-hover-theme mr-2">
                                <i class="flaticon-chat-1"></i><br>
                                <span>Ucapan</span>
                            </a>

                            <!--end::Actions-->
                        </div>
                        <!--end::Nav-->
                    </div>

                    <!--end::Container-->
                </div>

                <!--end::Footer-->
                <!--begin::Content-->
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <!--begin::Subheader-->
                    <!--end::Subheader-->
                    <!--begin::Entry-->
                    <div class="d-flex flex-column-fluid">
                        <!--begin::Container-->
                        <div class="container">
                            <section id="cover">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-5 pt-30">
                                                <div class="cover-wedding-invitation text-center mt-15">
   
                                                    <span><em>Wedding Invitation</em></span>
                                                    <h1 class="cover-title mt-3"><?=$data->bride_nickname?> &amp; <?=$data->groom_nickname?></h1>
                                                    <span><em><?=format_datetime($data->date)?></em></span>
                                                </div>
                                                <div class="text-center save-the-date">

                                                </div>


                                                <div class="text-center cover-to mt-10">
                                                    Kepada
                                                    <h5>
                                                        rivai sali
                                                    </h5>

                                                </div>

                                                <div class="button-open text-center">
                                                    <a href="#main" class="btn btn-theme btn-pill">Buka Undangan</a>
                                                </div>
                                                <div class="qrcode text-center mb-20">

                                                    <input type="hidden" id="codeTamu" value="">
                                                    <div id="qrcode"></div>
                                                    <div class="qrcode-info"><small>Gunakan QR Code untuk
                                                            check-in</small></div>
                                                </div>




                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>
                            <section id="main">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-5 pt-30">
                                                <div class="cover-wedding-invitation text-center">
                                                    <h1 class="cover-title mt-6 color-accent"><small><small>The
                                                                Wedding</small></small></h1>
                                                    <p>WE INVITE YOU TO CELEBRATE OUR WEDDING</p>
                                                </div>
                                                <div class="flex-shrink text-center main-img">
                                                    <img class="main-img-decoration"
                                                        src="<?php base_url() ?>/assets/theme/green/img/main-decor.png"
                                                        alt="image">
                                                    <div
                                                        class="symbol symbol-circle symbol-lg-180 symbol-md-150 symbol-sm-150 symbol-xs-150">
                                                        <img class="main-img-cover"
                                                            src="https://monku.id/uploads/gallery/<?=$header->image?>"
                                                            alt="image">
                                                    </div>
                                                </div>
                                                <div class="text-center cover-to">
                                                    <h1 class="cover-title color-theme"><?=$data->bride_nickname?> &amp; <?=$data->groom_nickname?></h1>
                                                </div>
                                                <div class="main-date text-center">
                                                    <div><?=format_datetime($data->date)?></div>
                                                    <div id="clock"></div>
                                                    <div id="countdown" class="badge badge-success"></div>
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>
                            <section id="bridegroom">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-20 pt-30">
                                                <div class="cover-wedding-invitation text-center">
                                                    <p>Maha Suci Allah yang telah menciptakan makhluk-Nya
                                                        berpasang-pasangan. Kami mohon do'a restu atas pernikahan kami:
                                                    </p>
                                                </div>
                                                <div class="row mb-15">
                                                    <div class="col-md-6 text-center mt-15">
                                                        <div class="flex-shrink text-center">
                                                            <div
                                                                class="symbol symbol-circle symbol-lg-150 symbol-md-150 symbol-sm-100">
                                                                <img src="https://monku.id/uploads/bridegroom/<?=$data->bride_photo?>"
                                                                    alt="image">
                                                            </div>
                                                        </div>
                                                        <h3><?=$data->bride_name?></h3>
                                                        <span><em>The Bride</em></span>
                                                        <p class="mt-5">&quot;<?=$data->bride_about?>&quot;</p>
                                                        <a href="https://www.instagram.com/<?=$data->bride_ig?>" target="_BLANK"
                                                            class="btn btn-theme">
                                                            <i class="fab fa-instagram"></i> <?=$data->bride_ig?>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6 text-center mt-15">
                                                        <div class="flex-shrink text-center">
                                                            <div
                                                                class="symbol symbol-circle symbol-lg-150 symbol-md-150 symbol-sm-100">
                                                                <img src="https://monku.id/uploads/bridegroom/<?=$data->groom_photo?>"
                                                                    alt="image">
                                                            </div>
                                                        </div>
                                                        <h3><?=$data->groom_name?></h3>
                                                        <span><em>The Groom</em></span>
                                                        <p class="mt-5">&quot;<?=$data->groom_about?>&quot;</p>
                                                        <a href="https://www.instagram.com/<?=$data->groom_ig?>" target="_BLANK"
                                                            class="btn btn-theme">
                                                            <i class="fab fa-instagram"></i><?=$data->groom_ig?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>
                            <section id="event">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-20 pt-30">
                                                <div class="cover-wedding-invitation text-center">
                                                    <h1 class="cover-title color-theme">Acara</h1>
                                                    <p>Kami bermaksud mengundang saudara dalam acara pernikahan kami</p>
                                                </div>
                                                <div class="row mb-15 justify-content-center">
                                                <?php foreach($event as $d): ?>
                                                    <div class="col-md-6 text-center mt-15">
                                                        <div class="card card-custom card-stretch gutter-b">
                                                            <div
                                                                style="height: 75px; width:100%; background: url(https://undangin.id/storage/schedules/48reAkGgbABMte8kA460qvL1XgKOc2w5fQ804cJW.jpeg) no-repeat center; background-size:cover">
                                                            </div>
                                                            <div class="card-body p-1 pt-5 pb-3">
                                                                <center>
                                                                    <h5 class="color-theme"><strong><?=$d->event_name?></strong>
                                                                    </h5>
                                                                    <span class=""><?=format_datetime($d->date)?></span><br>
                                                                    <small class="text-dark-75"><?=$d->time?> - Selesai
                                                                        WITA</small>
                                                                    <br> <br>
                                                                    <p>
                                                                        <b><?=$d->location_name?></b><br>
                                                                        <?=$d->location_address?>
                                                                    </p>
                                                                    <a href="https://www.google.com/maps/place/0%C2%B034'27.0%22N+122%C2%B055'43.4%22E/@0.574155,122.9265223,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d0.574155!4d122.928711"
                                                                        target="_blank"
                                                                        class="btn btn-pill btn-theme mb-3">Petunjuk
                                                                        Arah</a>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>

                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>

                            <section id="comment">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-20 pt-30">
                                                <div class="cover-wedding-invitation text-center">
                                                    <h1 class="cover-title color-theme">Ucapan &amp; Doa</h1>
                                                    <p></p>
                                                </div>
                                                <div class="row mb-15 justify-content-center">
                                                    <div class="col-md-12">
                                                        <?php
                            $kode_undangan = null;
                            if($kode_undangan==null){ ?>

                                                        <form id="guestbookConfirm" class=" bg-color-light p-4 h-100"
                                                            action="verify" method="POST">
                                                            <div class="form-group">

                                                                <input type="text"
                                                                    class="form-control form-control-custom" name="code"
                                                                    placeholder="Masukan Kode Undangan"
                                                                    onkeyup="myCode()"
                                                                    data-msg-required="This field is required."
                                                                    id="code" required="">
                                                            </div>
                                                            <input class="btn btn-theme btn-block" type="submit"
                                                                name="masuk" value="Aktifkan Komentar">
                                                        </form>

                                                        <?php }else{ ?>



                                                        <form id="guestbookSendMessage"
                                                            class=" bg-color-light p-4 h-100" action="sendmessage"
                                                            method="POST">
                                                            <input type="hidden" name="kode"
                                                                value="<?=$this->uri->segment(1)?>">
                                                            <div class="form-group">
                                                                <textarea class="form-control form-control-custom"
                                                                    style="height: 150px;" name="message"
                                                                    placeholder="Tulis Ucapan/Harapan/Komentar*"
                                                                    data-msg-required="This field is required."
                                                                    id="message" required=""></textarea>
                                                            </div>
                                                            <input class="btn btn-theme btn-block" type="submit"
                                                                value="Kirim" name="kirim">
                                                        </form>
                                                        <?php
                            } ?>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div data-scroll="true" data-height="500">
                                                            <div class="timeline timeline-justified timeline-4">
                                                                <div class="timeline-bar"></div>

                                                                <div class="timeline-items">
                                                                    <div class="timeline-item">
                                                                        <div class="timeline-badge">
                                                                            <div class="bg-success"></div>
                                                                        </div>

                                                                        <div class="timeline-label">
                                                                            <span
                                                                                class="color-theme font-weight-bold">waktu</span>
                                                                        </div>

                                                                        <div class="timeline-content">
                                                                            <strong>dfds</strong>
                                                                            <p>
                                                                                <dfsf</p> </div> </div> </div> </div>
                                                                                    </div> </div> </div> </div>
                                                                                    <!--end::Body-->
                                                                        </div>
                                                                        <!--end: Card-->
                                                                        <!--end::List Widget 8-->
                                                                    </div>
                                                                </div>
                            </section>
                            <section id="story">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!-- begin::List Widget 8 -->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!-- begin::Header
                end::Header
                begin::Body -->
                                            <div class="card-body p-20 pt-30">
                                                <div class="cover-wedding-invitation text-center">
                                                    <h1 class="cover-title color-theme">Galeri</h1>
                                                    <p></p>
                                                </div>
                                                <div class="row mb-15 justify-content-center">

                                                <?php foreach ($gallery as $g) : ?>
                                                    <div class="col-md-6 mt-3">
                                                        <div class="card card-custom overlay">
                                                            <div class="card-body p-0" data-toggle="popover"
                                                                title="<?=$g->title?>" data-html="true"
                                                                data-content="<?=$g->title?>">
                                                                <div class="overlay-wrapper">
                                                                    <img src="https://monku.id/uploads/gallery/<?=$g->image?>"
                                                                        alt="" class="w-100 rounded" />
                                                                </div>
                                                                <div
                                                                    class="overlay-layer m-5 rounded align-items-end justify-content-start">
                                                                    <div
                                                                        class="d-flex flex-column align-items-start mb-5 ml-5">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                           <?php endforeach; ?>

                                                </div>
                                            </div>
                                            <!-- end::Body -->
                                        </div>
                                        <!-- end: Card
                                        end::List Widget 8 -->
                                    </div>
                                </div>
                            </section>
                            <section id="protocols">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-20 pt-30">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="cover-wedding-invitation text-center">
                                                            <h1 class="cover-title color-theme">Protokol Kesehatan</h1>
                                                        </div>
                                                        <div class="row mb-15 justify-content-center">
                                                            <div class="col-md-12 text-center">
                                                                <blockquote class="blockquote text-center">
                                                                    <p class="mb-0">Mengingat kondisi pandemi saat ini, kami menghimbau Bapak/Ibu/Saudara/i tamu undangan
                                                                         agar tetap memperhatikan protokol kesehatan dalam rangka upaya pencegahan penyebaran virus Covid-19.</p>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-center mt-5">
                                                        <div class="flex-shrink text-center">
                                                        <img src="<?php base_url() ?>/assets/img/protocol_covid.png"
                                                                    alt="image">
                                                        </div>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>
                            <section id="quotes">
                                <div class="row justify-content-center">
                                    <div class="col-lg-6 col-md-8 col-sm-12">
                                        <!--begin::List Widget 8-->
                                        <div class="card card-custom card-stretch gutter-b border-purple main-bg">
                                            <!--begin::Header-->
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body p-20 pt-30">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="cover-wedding-invitation text-center">
                                                            <h1 class="cover-title color-theme">Quotes</h1>
                                                        </div>
                                                        <div class="row mb-15 justify-content-center">
                                                            <div class="col-md-12 text-center">
                                                                <blockquote class="blockquote text-center">
                                                                    <p class="mb-0">Ini awal cerita kami. Kami akan
                                                                        menulis cerita ini seindah mungkin. Mohon Doa
                                                                        Restu dari teman-teman semua</p>
                                                                    <div class="blockquote-footer"><?=$data->bride_nickname?> &amp; <?=$data->groom_nickname?><br>
                                                                        <cite
                                                                            title="Source Title">#<?=$data->bride_nickname.$data->groom_nickname?>Wedding</cite>
                                                                    </div>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-center mt-5">
                                                        <div class="flex-shrink text-center">
                                                            <div
                                                                class="symbol symbol-circle symbol-lg-150 symbol-md-150 symbol-sm-100">
                                                                <img src="https://monku.id/uploads/gallery/<?=$header->image?>"
                                                                    alt="image">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 text-center mt-5">
                                                        <p></p>
                                                        <p>Hormat Kami <br><br><strong><?=$data->groom_nickname?> &amp; <?=$data->bride_nickname?></strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end: Card-->
                                        <!--end::List Widget 8-->
                                    </div>
                                </div>
                            </section>
                            <div class="row">
                                <div class="col text-center mb-20">
                                    <p><strong><?=$data->groom_nickname?> &amp; <?=$data->bride_nickname?> Wedding Web</strong><br>
                                        <style>
                                            .heart {
                                                color: #e25555;
                                            }
                                        </style>Made with <span class="heart">❤</span> somewhere in the world.
                                    </p>&copy; Developed By <a href="http://monku.id">monku.id</a>
                                </div>
                            </div>
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->
    <!-- begin::User Panel-->

    <!-- end::User Panel-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop">
        <span class="svg-icon">
            <!--begin::Svg Icon | path:media/svg/icons/Navigation/Up-2.svg-->
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
                    <path
                        d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z"
                        fill="#000000" fill-rule="nonzero" />
                </g>
            </svg>
            <!--end::Svg Icon-->
        </span>
    </div>
    <!--end::Scrolltop-->
    <!--begin::Sticky Toolbar-->
    <ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
        <!--begin::Item-->
        <li class="nav-item" id="kt_sticky_toolbar_chat_toggler">
            <a class="btn btn-sm btn-icon btn-bg-light btn-icon-theme btn-hover-theme" href="#" data-toggle="modal"
                data-target="#backsoundModal">
                <i class="flaticon-music"></i>
            </a>
        </li>
        <!--end::Item-->
    </ul>


    <div class="modal fade" id="backsoundModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content invitation-bg">
                <div class="modal-header">
                    <h4 class="modal-title invitation-font" id="defaultModalLabel">Music</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body wedding-font">
                    <center>
                        <button class="btn btn-theme btn-icon mr-3" id="btn-play" onclick="play()"><i
                                class="fa fa-play"></i></button>
                        <button class="btn btn-theme btn-icon mr-3" id="btn-pause" onclick="pause()"><i
                                class="fa fa-pause"></i></button>
                        <button class="btn btn-theme btn-icon" id="btn-stop" onclick="stop()"><i
                                class="fa fa-stop"></i></button>
                    </center>
                </div>
                <div class="modal-footer text-center p-7">
                    <button type="button" class="btn btn-theme btn-block" data-dismiss="modal"
                        aria-hidden="true">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="donationModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel"
        style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content bg-white">
                <form target="_blank" class="form" action="proses-pembayaran" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Kirim Kado (Cashless) untuk Ayhu &amp; Pudin</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body wedding-font">
                        <div class="form-group">
                            <label>Jumlah<br><small>Jumlah minimal Rp 10.000 &amp; akan ditambah biaya admin Rp
                                    5.000</small></label>
                            <input type="number" name="amount" class="form-control bg-white" min="10000"
                                id="donation-amount" onkeyup="checkAmount()" required />
                            <input type="hidden" name="code" value="" />
                            <input type="hidden" name="nama_lengkap" value="" />
                            <input type="hidden" name="no_telp" value="" />
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="hidden" value="true"
                                        title="Identitas anda akan disembunyikan dari calon pasangan pengantin" />
                                    Sembunyikan identitas</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Metode Pembayaran</label>
                            <div class="radio-list payment-method">
                                <label class="radio" style="width: 100%;">
                                    <input type="radio" name="payment_method" value="banktransfer" required />
                                    <div class="bank-method" style="display: inline-block;width: 120px;">
                                        Transfer Bank
                                    </div>
                                    <img src="<?=base_url()?>/assets/img/logos/bca.jpg" class="bank-logo"
                                        style="max-width: 200px;">
                                    <span></span>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="payment_method" value="va" />
                                    <div class="bank-method" style="display: inline-block;width: 120px;">
                                        Virtual Account
                                    </div>
                                    <img src="<?=base_url() ?>/assets/img/logos/va.jpg" class="bank-logo"
                                        style="max-width: 200px;">
                                    <span class="bank-logo-va"></span>
                                </label>
                                <label class="radio" id="radio-qris">
                                    <input type="radio" name="payment_method" id="qris-input" value="qris" />
                                    <div class="bank-method" style="display: inline-block;width: 120px;">
                                        QRIS
                                    </div>
                                    <img src="<?=base_url() ?>/assets/img/logos/qris.jpg" class="bank-logo"
                                        style="max-width: 200px;">
                                    <span class="bank-logo-qris"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit"
                            class="btn btn-theme btn-xs custom-border-radius text-3 text-uppercase text-color-light outline-none p-3 pl-5 pr-5">Lanjutkan
                            ke pembayaran</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--end::Sticky Toolbar-->
    <script>
        var HOST_URL = "https://keenthemes.com/metronic/tools/preview";
    </script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1200
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#3699FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#F3F6F9",
                        "dark": "#212121"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1F0FF",
                        "secondary": "#ECF0F3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#212121",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#ECF0F3",
                    "gray-300": "#E5EAEE",
                    "gray-400": "#D6D6E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#80808F",
                    "gray-700": "#464E5F",
                    "gray-800": "#1B283F",
                    "gray-900": "#212121"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="<?php base_url() ?>/assets/plugins/global/plugins.bundle.js"></script>
    <script src="<?php base_url() ?>/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
    <script src="<?php base_url() ?>/assets/js/scripts.bundle.js"></script>
    <!--end::Global Theme Bundle-->
    <!--begin::Page Vendors(used by this page)-->
    <script src="<?php base_url() ?>/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="<?php base_url() ?>/assets/js/pages/widgets.js"></script>
    <script src="<?php base_url() ?>/assets/plugins/custom/countdown/jquery.countdown.min.js"></script>
    <script src="<?php base_url() ?>/assets/plugins/custom/instahistory/instastory.js"></script>
    <!--end::Page Scripts-->
    <script>
        var resepsiDate = new Date("Sept 26, 2021 10:00:00").getTime();

        var x = setInterval(function () {

            var now = new Date().getTime();
            var distance_resepsi = resepsiDate - now;

            if (distance_resepsi < 0) {
                clearInterval(x);
                $('#clock').hide();
                document.getElementById("countdown").innerHTML = "Happily Ever After";
            }


        }, 1000);

        var time = "2021/09/26 10:00:00";
        $('#clock').countdown(time, function (event) {
            var $this = $(this).html(event.strftime('' +
                '<div class=""><span class="label label-md font-weight-bold label-rounded label-countdown">%D</span><br><small>Hari</small></div>' +
                '<div><span class="label label-md font-weight-bold label-rounded label-countdown">%H</span><br><small>Jam</small></div>' +
                '<div><span class="label label-md font-weight-bold label-rounded label-countdown">%M</span><br><small>Menit</small></div>' +
                '<div><span class="label label-md font-weight-bold label-rounded label-countdown">%S</span><br><small>Detik</small></div>'
                ));
        });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php base_url() ?>/assets/plugins/custom/howler/dist/howler.js"></script>
    <script>
        function gotoInvitation() {
            let code = $('#invitationCode').val();
            location.replace('/' + code);
        }

        function hideModal(element) {
            $(element).modal('hide');
        }
 
        $('#feed').instastory({
            get: '#03032403032',
            imageSize: 240,
            limit: 12,
            template: '<div class="igcol"><a href="\{\{link\}\}" target="_blank"><img src="\{\{image\}\}"><span class="iginfo">❤️ \{\{likes\}\} 💬 \{\{comments\}\}</span></a></div>'
        });
   
        let autoplay = true;
        var sound = new Howl({
            src: ['<?php base_url() ?>/assets/backsounds/backsound3.mp3'],
            autoplay: autoplay,
            loop: true,
            // volume: 0.5,
            onend: function () {
                console.log('Finished!');
            },
            onplayerror: function () {
                sound.once('unlock', function () {
                    stop()
                    play()
                });
            },
            onplay: function () {
                $('#btn-play').prop("disabled", true);
                $('#btn-pause').prop("disabled", false);
            }
        });
        $('#btn-pause').prop("disabled", true);

        function play() {
            stop()
            sound.play();
        }


        function pause() {
            sound.pause();
            $('#btn-play').prop("disabled", false);
            $('#btn-pause').prop("disabled", true);
        }

        function stop() {
            sound.stop();
            $('#btn-play').prop("disabled", false);
            $('#btn-pause').prop("disabled", true);
        }

        function myCode() {
            var x = document.getElementById("code");
            x.value = x.value.toUpperCase();
        }

        function checkAmount() {
            let amount = $('#donation-amount').val();
            if (amount > 150000) {
                $('#qris-input').attr('disabled', true);
                $('#qris-input').prop('checked', false);
                $('#radio-qris').addClass('radio-disabled');
            } else {
                $('#qris-input').attr('disabled', false);
                $('#radio-qris').removeClass('radio-disabled');
            }
        }



        function popup_gift() {
            Swal.fire({
                position: 'center',
                // icon: 'error',
                title: "Selamat anda mendapatkan pulsa 5.000",
                showConfirmButton: false,
                timer: 4000,
                background: '#fff url(https://sweetalert2.github.io/images/trees.png)',
                backdrop: `
    rgba(0,0,123,0.4)
    url("https://media.giphy.com/media/PMV7yRpwGO5y9p3DBx/source.gif")
    center top
    no-repeat`,
            }).then(
                function () {
                    $("#checkin").attr("disabled", true);
                    $("#pencarian").focus();
                }

            )

        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?php base_url() ?>/assets/js/jquery.qrcode.js"></script>
    <script src="<?php base_url() ?>/assets/js/qrcode.js"></script>
    <script>
        jQuery(function () {
            var code = $("#codeTamu").val();
            jQuery('#qrcode').qrcode({
                width: 84,
                height: 84,
                text: code
            });
        });
    </script>
    <!--</body></html>-->
</body>
<!--end::Body-->

</html>