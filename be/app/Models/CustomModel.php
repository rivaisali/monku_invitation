<?php namespace App\Models;

class CustomModel
{
    protected $db;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function check_id($table = null, $field = null, $where = false)
    {
        $builder = $this->db->table($table);
        $builder->select('*');
        if ($where) {
            $builder->where($where);
        }
        $builder->orderBy($field, "DESC");
        $builder->limit(1);
        $query = $builder->get();
        $check = $query->getRow();
        if (empty($check)) {
            return "1";
        } else {
            $data = $check->$field + 1;
            return $data;
        }
    }

    // select
    public function select_data($table, $return = "getResult", $where = false, $like = null, $order = null, $limit = false, $group = null)
    {

        $builder = $this->db->table($table);
        $builder->select('*');
        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }

        if ($like !== null) { // jika like memiliki nilai array
            foreach ($like as $like => $like_by) {
                $builder->like($like, $like_by);
            }
        }

        if ($order !== null) { // jika order memiliki nilai
            foreach ($order as $order => $order_by) {
                $builder->orderBy($order, $order_by);
            }
        }

        if ($limit) { // jika limit memiliki nilai
            $builder->limit($limit);
        }

        if ($group !== null) { // jika group memiliki nilai
            foreach ($group as $group) {
                $builder->groupBy($group);
            }
        }
        $query = $builder->get();
        return $query->$return();
    }

    // select
    public function select_data_custom($table, $return = "getResult", $where = false, $wherein = false)
    {

        $builder = $this->db->table($table);
        $builder->select('*');

        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }

        if ($wherein) { // jika where memiliki nilai array
            $builder->whereNotIn('id', $wherein);

        }

        $query = $builder->get();
        return $query->$return();
    }

    // select for update
    public function select_data_forupdate($table, $select = "*", $return = "getResult", $where = false)
    {

        $builder = $this->db->table($table);
        $sql = $builder->select($select)
            ->where($where)
            ->getCompiledSelect(false);
        $query = $sql . " FOR UPDATE";
        return $this->db->query($query)->$return();

    }

    public function select_data_count($table, $return = "getResult", $where = false, $like = null, $order = null, $limit = false, $limit2 = false, $group = null)
    {
        $builder = $this->db->table($table);
        $builder->select('count(*) as count');
        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }

        if ($like !== null) { // jika like memiliki nilai array
            foreach ($like as $like => $like_by) {
                $builder->like($like, $like_by);
            }
        }

        if ($order !== null) { // jika order memiliki nilai
            foreach ($order as $order => $order_by) {
                $builder->orderBy($order, $order_by);
            }
        }

        if ($limit) { // jika limit memiliki nilai
            $builder->limit($limit);
        }

        if ($group !== null) { // jika group memiliki nilai
            foreach ($group as $group) {
                $builder->groupBy($group);
            }
        }
        $query = $builder->get();
        return $query->$return();
    }

    public function select_data_count_join($table, $return = "getResult", $join = null, $where = false, $like = null, $order = null, $limit = false, $group = null)
    {
        $builder = $this->db->table($table);
        $builder->select('count(*) as count');

        if ($join !== null) { // jika join memiliki nilai array
            foreach ($join as $jo) {
                $builder->join($jo["table"], $jo["cond"], $jo["type"]);
            }
        }

        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }

        if ($like !== null) { // jika like memiliki nilai array
            foreach ($like as $like => $like_by) {
                $builder->like($like, $like_by);
            }
        }

        if ($order !== null) { // jika order memiliki nilai
            foreach ($order as $order => $order_by) {
                $builder->orderBy($order, $order_by);
            }
        }

        if ($limit) { // jika limit memiliki nilai
            $builder->limit($limit);
        }

        if ($group !== null) { // jika group memiliki nilai
            foreach ($group as $group) {
                $builder->groupBy($group);
            }
        }
        $query = $builder->get();
        return $query->$return();
    }

    // select join
    public function select_data_join($select = "*", $table = null, $return = "getResult", $join = null, $where = false, $like = null, $order = null, $group = null, $limit = false, $offset = false)
    {
        $builder = $this->db->table($table);
        $builder->select($select);
        if ($join !== null) { // jika join memiliki nilai array
            foreach ($join as $jo) {
                $builder->join($jo["table"], $jo["cond"], $jo["type"]);
            }
        }

        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }

        if ($like !== null) { // jika like memiliki nilai array
            foreach ($like as $like => $like_by) {
                $builder->like($like, $like_by);
            }
        }

        if ($order !== null) { // jika order memiliki nilai
            foreach ($order as $order => $order_by) {
                $builder->orderBy($order, $order_by);
            }
        }

        if ($limit) { // jika limit memiliki nilai
            $builder->limit($limit, $offset);

        }

        if ($group !== null) { // jika group memiliki nilai
            foreach ($group as $group) {
                $builder->groupBy($group);
            }
        }
        $query = $builder->get();
        return $query->$return();
    }

    // select custom query
    public function select_custom($query, $return = "getResult")
    {
        $query = $this->db->query($query);
        return $query->$return();
    }



    // select max
    public function select_function($function, $table, $field, $where = false)
    {
        $builder = $this->db->table($table);
        $builder->$function($field);
        if ($where) { // jika where memiliki nilai array
            $builder->where($where);
        }
        $query = $builder->get();
        return $query->getRow();
    }

    public function insert($table, $data)
    {
        $builder = $this->db->table($table);
        $q = $builder->insert($data);
        // $q    =    $this->insert($table, $data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function update($table, $data, $where)
    {
        $builder = $this->db->table($table);
        $builder->where($where);
        $q = $builder->update($data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($table, $where)
    {

        $builder = $this->db->table($table);
        $builder->where($where);
        $del = $builder->delete();
        if ($del) {
            return true;
        } else {
            return false;
        }
    }
}
