<?php
header("Content-type: text/css; charset: UTF-8");

?>
html {
    scroll-behavior: smooth;
  }
.bg{
    background: url('<?=$base_url.'assets/'.$dir_theme.'/img/bg.jpg'?>') no-repeat center;
    background-size: 100% 100%;
    min-height: 500px;
}

.cover-wedding-invitation{
    margin-top: 1rem;
}
.qrcode p{
    color: <?=$primaryColor?>;
}
.cover-title{
    font-family: 'Great Vibes', cursive;
    font-size: 2.5rem;
    color: #161616;
}
.save-the-date{
    margin-top: 3.7rem;
}
.save-the-date img{
    height: 150px;
}
.cover-to{
    margin-top: 3.7rem;
}
.qrcode-info{
    padding-bottom: 30px;
}
.button-open,.qrcode{
    margin-top: 3.7rem;
}
/* .border-purple{
    border: 0.3rem solid <?=$primaryColor?> !important;
} */
.main-bg{
    background-image:   url('<?=$base_url.'assets/'.$dir_theme.'/img/bl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/br.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/btc.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/tl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/tr.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/bg_paper.png'?>');
    background-position: bottom left, bottom right, top center, top left, top right, center;
    background-repeat: no-repeat, no-repeat, no-repeat, no-repeat, no-repeat, repeat;
    background-size: 200px, 200px, 80px, 200px, 200px, auto;
}
.color-theme{
    color: #1d1d1d;
}
.color-accent{
    color: #4c6d5a;
}
.color-black{
    color: #000;
}
.main-img .main-img-cover{
    /* position: absolute; */
    padding: 15px;
}
.main-img-decoration{
    position: absolute;
    width: 100%;
    max-width: 150px;
    height: 150px;
    z-index: 1;
}
.main-date{
    margin-top: 0rem;
    margin-bottom: 3rem;
}
.label-countdown{
    background-color: <?=$primaryColor?>;
    color: #fff;
    padding: 15px;
    margin: 2px;
    font-size: 14px;
}
#clock div{
    display: inline-block;
}
.btn-theme{
    background-color: <?=$primaryColor?>;
    border-color: <?=$primaryColor?>;
    color: #fff;
}
.btn-theme i{
    color: #fff;
}

.btn-theme:hover{
    background-color: <?=$primaryColor?>;
    border-color: <?=$primaryColor?>;
    color: #fff;
}
#backsoundModal iframe{
    width: 100% !important;
}
.btn-icon-theme i{
    color: <?=$primaryColor?>;
}
.send-gift-bg{
    background-image: url('<?=$base_url.'assets/'.$dir_theme.'/img/send_gift.png'?>');
    background-repeat: no-repeat;
    background-position: bottom left;
    background-size: 200px;
}
.timeline.timeline-4 .timeline-items .timeline-item .timeline-content {
    background: #d9e9e2;;
}
.timeline.timeline-4.timeline-justified .timeline-items .timeline-item:after {
    border-right: solid 10px #d9e9e2;
}
#bottom-navigation {
    overflow: hidden;
    position: fixed;
    bottom: 0;
    width: 100%;
    z-index: 100;
}

.scrolltop {
    bottom: 90px;
}
.btn.btn-hover-theme:not(:disabled):not(.disabled):active:not(.btn-text), .btn.btn-hover-theme:not(:disabled):not(.disabled).active, .show > .btn.btn-hover-theme.dropdown-toggle, .show .btn.btn-hover-theme.btn-dropdown {
    color: #FFFFFF !important;
    background-color: <?=$primaryColor?> !important;
    border-color: <?=$primaryColor?> !important;
}
.btn.btn-light-theme {
    color: <?=$primaryColor?>;
    background-color: #d9e9e2;
    border-color: transparent;
}
.btn.btn-light-theme i{
    color: <?=$primaryColor?>;
}
@media only screen and (max-width: 720px){
    .cover-title{
        font-size: 2.7rem;
    }
    .save-the-date img{
        height: 200px;
    }
    .save-the-date{
        margin-top: 2.5rem;
    }
    .cover-to{
        margin-top: 2.5rem;
    }
    .qrcode-info{
        padding: 0px 30px 30px 30px;
    }
    .main-bg{
        background-image:   url('<?=$base_url.'assets/'.$dir_theme.'/img/bl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/br.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/btc.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/tl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/tr.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/bg_paper.png'?>');
                            
        background-position: bottom left, bottom right, top center, top left, top right, center;
        background-repeat: no-repeat, no-repeat, no-repeat, no-repeat, no-repeat, repeat;
        background-size: 150px, 150px, 30px, 150px, 150px, auto;
    }
    .main-img img {
        margin-top: 2rem;
        width: 100%;
        max-width: 150px;
        height: 150px;
    }
    .symbol-sm-150 img {
        width: 100%;
        max-width: 150px;
        height: 150px;
    }
    .symbol-sm-100 img {
        width: 100%;
        max-width: 100px;
        height: 100px;
    }
    .scrolltop {
        bottom: 90px;
    }
}


@media only screen and (max-width: 360px){
    .cover-title{
        font-size: 2.2rem;
    }
    .save-the-date img{
        height: 80px;
    }
    .save-the-date{
        margin-top: 1rem;
    }
    .cover-to{
        margin-top: 1rem;
    }
    .button-open, .qrcode{
        margin-top: 1rem;
    }
    /* .main-bg{
        background-image:   url('<?=$base_url.'assets/'.$dir_theme.'/img/bl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/br.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/btc.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/bg_paper.png'?>');
        background-position: bottom left, bottom right, top center, center;
        background-repeat: no-repeat, no-repeat, no-repeat, repeat;
        background-size: 100px, 100px, 30px, auto;
    } */
    
    .pt-30{
        padding-top: 5rem !important;
    }
}

@media only screen and (max-width: 320px){
    .cover-title{
        font-size: 2rem;
    }
    .save-the-date img{
        height: 75px;
    }
    .save-the-date{
        margin-top: 1rem;
    }
    .cover-to{
        margin-top: 1rem;
    }
    .button-open, .qrcode{
        margin-top: 1rem;
    }
    .qrcode-info{
        padding: 0px 30px 30px 30px;
    }
    .symbol-xs-100 img {
        width: 100%;
        max-width: 100px;
        height: 100px;
    }
    /* .main-bg{
        background-image:   url('<?=$base_url.'assets/'.$dir_theme.'/img/bl.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/br.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/btc.png'?>'),
                        url('<?=$base_url.'assets/'.$dir_theme.'/img/bg_paper.png'?>');
        background-position: bottom left, bottom right, top center, center;
        background-repeat: no-repeat, no-repeat, no-repeat, repeat;
        background-size: 100px, 100px, 30px, auto;
    } */
}